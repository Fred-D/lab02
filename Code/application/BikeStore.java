//Fred-Dgennie Louis 2036915
//https://gitlab.com/Fred-D/lab02.git
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] myBicycles = new Bicycle[4];

        myBicycles[0] = new Bicycle("RedRoad", 21, 40);
        myBicycles[1] = new Bicycle("Mountain Killer", 25, 35);
        myBicycles[2] = new Bicycle("ParKour", 22, 45);
        myBicycles[3] = new Bicycle("Champions", 30, 50);

        //loop to print my bicycles

        for (int i=0; i< myBicycles.length; i++) {
            System.out.println(myBicycles[i]);
        }
    }
        
    
}
