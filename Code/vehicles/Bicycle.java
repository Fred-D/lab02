//Fred-Dgennie Louis 2036915
//https://gitlab.com/Fred-D/lab02.git
package vehicles;
public class Bicycle {   
    
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    
    //class constructor
    public Bicycle(String inputManufacturer, int inputNumberGears, double inputMaxSpeed) {
        this.manufacturer = inputManufacturer;
        this.numberGears = inputNumberGears;
        this.maxSpeed = inputMaxSpeed;

    }

    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed;
    }

}